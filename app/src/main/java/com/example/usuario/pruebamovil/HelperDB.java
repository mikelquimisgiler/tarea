package com.example.usuario.pruebamovil;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public class HelperDB extends SQLiteOpenHelper {

    private static final String TABLE_CONTROL_DATOS =
            "CREATE TABLE  parametros (ciudad text,  nombre text, capacidad text, fecha text)";

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "MiBasedeDatos.db";


    public HelperDB(Context context) {
        super(context, DATABASE_NAME, null,  DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CONTROL_DATOS);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS parametros");
        onCreate(db);

    }

    public String LeerTodo() {

        String consulta = "";
        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT * FROM parametros", null);
        if (cursor.moveToFirst()){
            do{
                String Estuciudad = cursor.getString(cursor.getColumnIndex("ciudad"));
                String EstuNombre = cursor.getString(cursor.getColumnIndex("nombre"));
                String Estucapacidad = cursor.getString(cursor.getColumnIndex("capacidad"));
                String Estufecha = cursor.getString(cursor.getColumnIndex("fecha"));

                consulta += "Ciudad:" + Estuciudad + "\n" + "Nombre:" + EstuNombre + "\n" + "Capacidad::" + Estucapacidad +  "\n" + "Fecha:" + Estufecha  +  "\n";
            }while (cursor.moveToNext());
        }
        return consulta;
    }
}
